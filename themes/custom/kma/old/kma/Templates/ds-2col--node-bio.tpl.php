<?php

/**
 * @file
 * Display Suite 2 column template.
 */
?>
<<?php print $layout_wrapper; print $layout_attributes; ?> class="ds-2col <?php print $classes;?> clearfix">

  <div class="Row">
  		<div class="col-md-9">
    		<?php print $left; ?>
    	</div>
  		<div class="col-md-3">
    		<?php print $right; ?>
    	</div>
  </div>

</<?php print $layout_wrapper ?>>

<?php if (!empty($drupal_render_children)): ?>
  <?php print $drupal_render_children ?>
<?php endif; ?>
