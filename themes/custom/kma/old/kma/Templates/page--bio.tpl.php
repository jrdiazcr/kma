<header class="navbar container-fluid navbar-default">
	<div class="container-fluid">
		<div class="row global-nav">
			<nav class="navbar navbar-form navbar-left pull-right global-search" role="navigation">
					<?php print render($page['top_navigation']); ?>
		 	</nav>
		</div>

 	  	<div class="row main-nav">
		      <div role="navbar-header">
		      <a href="<?php print $GLOBALS['base_url']; ?>">
		        		<img src="/<?php print $directory;?>/assets/img/kma-logo-color-small.png" alt="KMA" class="pull-left kma-logo"></a>
				<button class="navbar-toggle" data-target=".navbar-collapse" data-toggle="collapse" type="button">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			  </div>
			  <div class="navbar-collapse collapse">
			  	<nav role="navigation">
				<?php // --> Commented because menu will be presented with Child Items
					//print theme('links__system_main_menu', array('links' => $main_menu, 'attributes' => array('class' => array('links', 'nav', 'navbar-nav', 'pull-right', 'my-nav', 'inline', 'clearfix'))));?>
					<?php
					$menu_name = variable_get('menu_main_links_source', 'main-menu');
					drupal_static_reset('menu_tree');
					$tree = menu_tree($menu_name);
					print drupal_render($tree);
					?>
			   </nav>
			 </div>
		</div>
</header>
<?php
// Slide Image is not presented in all pages, there is a context that decides in what pages is presented
if ($page['slide_image']):
  print render($page['slide_image']);
endif;
?>
<div class="container-fluid main-content bio">
  <section class="container-fluid">
    <div class="row center-block container">
      <div class="col-md-12">

		<?php if ($tabs['#primary'] != ''): ?>
			<div id="tabs-wrapper" class="clearfix"><?php print render($tabs); ?></div>
		<?php endif; ?>

          <?php print render($tabs2); ?>
          <?php print $messages; ?>

          <?php print render($page['help']); ?>
          <?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>

          <div class="clearfix">
		      <?php if ($page['sidebar']): ?>
				    <div class="container-fluid">
              <div class="row">
                <div class="col-md-9">
		      <?php endif; ?>

		      <?php
            print render($page['content']);
            print render($page['bottom_content']);
          ?>

			  <?php if ($page['sidebar']): ?>
                </div>

                <div class="col-md-3 sidebar-content">
                  <?php print render($page['sidebar']); ?>
                </div>
              </div>
            </div>
		    <?php endif; ?>

			  <?php if ($page['highlighted']): ?>
          <div id="highlighted">
            <?php print render($page['highlighted']); ?>
          </div>
        <?php endif; ?>
          </div>
          <?php print $feed_icons ?>
      </div>
    </div>
  </section>

</div>

<footer class="container-fluid">
  <div class="row center-block container">
    <?php print render($page['footer']); ?>
  </div>
</footer>
<?php
?>
