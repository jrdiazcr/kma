<header class="navbar container-fluid navbar-default">
	<div class="container-fluid">
		<div class="row global-nav">
			<nav class="navbar navbar-form navbar-left pull-right global-search" role="navigation">
				<?php print render($page['top_navigation']); ?>
		 	</nav>
		</div>

 	  	<div class="row main-nav">
		      <div role="navbar-header">
		        <img src="/<?php print $directory;?>/assets/img/kma-logo-color-small.png" alt="KMA" class="pull-left kma-logo">
				<button class="navbar-toggle" data-target=".navbar-collapse" data-toggle="collapse" type="button">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			  </div>
			  <div class="navbar-collapse collapse">
			  	<nav role="navigation">
				<?php // --> Commented because menu will be presented with Child Items
					//print theme('links__system_main_menu', array('links' => $main_menu, 'attributes' => array('class' => array('links', 'nav', 'navbar-nav', 'pull-right', 'my-nav', 'inline', 'clearfix'))));?>
					<?php
					$menu_name = variable_get('menu_main_links_source', 'main-menu');
					drupal_static_reset('menu_tree');
					$tree = menu_tree($menu_name);
					print drupal_render($tree);
					?>
			   </nav>
			 </div>
		</div>
	</div>
</header>
<div class="navbar container-fluid navbar-default">
	<?php print render($page['slide_image']); ?>
	<section class="container-fluid main-content">
		<div class="row center-block container">
			<?php print $messages; ?>

        	<div class="col-md-6">
				<?php print render($page['left_content']); ?>
			</div>

			<div class="col-md-6 right-column">
				<?php print render($page['right_content']); ?>
			</div>
        </div>
 	</section>
	<section class="container-fluid main-content">
		<div class="row center-block container">
     		<?php if ($page['highlighted']): ?><div id="highlighted"><?php print render($page['highlighted']); ?></div><?php endif; ?>
    	</div>
	</section>
 </div>

<footer class="container-fluid">
  <div class="row center-block container">
    <?php print render($page['footer']); ?>
  </div>
</footer>
