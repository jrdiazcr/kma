<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the	 field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */

 // JD: This template file was created to change the layout presented in Our Clients according to the user requirements

 // Gets the total of rows of the view
 $total_rows = count($view->result);

 // Gets the current row of the view
 $current_row = $view->row_index + 1;

 if(isset($_SESSION['kma'])):
	$client_type = $_SESSION['kma']['client_type']; 
else:
	$client_type = '';
endif;

// If it is the first record, creates the container div
if ($current_row == 1):
	print '<div class="container-fluid"><div class="row">';
endif;

foreach ($fields as $id => $field):

	$current_Client_Type = removeHTMLTags($field->content, 2);

	if ($field->label == "Type"):
		if ($current_row == 1):
			print $field->content;
		else:

			if ($client_type != $current_Client_Type):
				print '</div><div class="row">';
				print $field->content;
			endif;
		endif;

		$_SESSION['kma']['client_type'] = $current_Client_Type;
	else:
	 	if (!empty($field->separator)):
	    	print $field->separator;
	  	endif;

		if ($field->label == "Title"):
			print '<div class="row-view-item client">';
			print $field->content;
		endif;
	endif;

endforeach;

// Closes the Column Div
print '</div>';

// If the final row has being printed, it closes the Row and Container Divs
if ($current_row == $total_rows):
	 print '</div></div>';
endif;

?>
