<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the	 field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
?>



<?php 
print '<section class="whats-new-item-home clearfix">'; 
$closeH5Tag = FALSE;	
?>



<?php foreach ($fields as $id => $field): ?>
  <?php if (!empty($field->separator)): ?>
    <?php print $field->separator; ?>
  <?php endif; ?>


	<?php

	// Verifies if the previous field was the Content Type, if it is changes the class to present the correct icon
	if ($field->label == "[no_name]" ):
		if ($row->_field_data['nid']['entity']->type == "post"):
			$field->label_html = str_replace("icon-news-front", "icon-post", $field->label_html);
		elseif ($row->_field_data['nid']['entity']->type == "event"):
			$field->label_html = str_replace("icon-news-front", "icon-event", $field->label_html);
		endif;
	endif;

	// Verifies if there is a value for the Content and assures that Content Type Field will not be rendered
	if (!empty($field->content) && $field->label != "Content Type"):
		// Removes the variant [no_name] placed in the view to indicate that this Field will not have a label
		$field->label_html = str_replace("[no_name]", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	", $field->label_html);

		print $field->wrapper_prefix;
		
		switch ($field->label):
			case "Date":
				print '<h5>' . $field->content;
				$closeH5Tag = TRUE;
				break;
			case "Place":
				
				if ($field->content != ""):
					print " - ";
				endif;
				
				print $field->content . '</h5>';
				$closeH5Tag = FALSE;
				break;
			case "By ":
				
				if ($closeH5Tag == TRUE):
					print '</h5>'; 
				endif;
					
				print '<h5>' . $field->label_html . $field->content . '</h5>';
				break;
			default:

				if ($closeH5Tag == TRUE):
					print '</h5>'; 
				endif;
				
				print $field->content;
			  	break;
		endswitch;
		
		print $field->wrapper_suffix;
	endif;
endforeach;

?>
<?php print '</section>' ?>
