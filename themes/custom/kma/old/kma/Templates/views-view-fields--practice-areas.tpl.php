<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the	 field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */

 // JD: This template file was created to change the layout presented in Practice Areas according to the user requirements
 
 // Gets the total of rows of the view
 $total_rows = count($view->result);
 
 // Gets the current row of the view
 $current_row = $view->row_index + 1;

// If it is the first record, creates the container div
if ($current_row == 1):
	print '<div class="container-fluid"><div class="row">';
endif;

// Prints the Column Div 
print '<div class="col-md-4 grid-item">';
 
foreach ($fields as $id => $field):
 	if (!empty($field->separator)):
    	print $field->separator;
  	endif; 

	if ($field->label == "Path"):
		$path = removeHTMLTags($field->content, 1);
	elseif ($field->label == "Icon"):
		
		// Removes the HTML tags from the content				 
		$pos1 = stripos($field->content, ">");
		
		if ($pos1 === false):
			$iconClass = $field->content;
		else:
			$pos2 = stripos($field->content, "<", $pos1);
			$iconClass = substr($field->content, $pos1 + 1, $pos2 - $pos1 - 1);	
		endif;

		print '<a href="' . $path .  '"><span class="caret headline-icon-secondary icon-' . $iconClass . '"></span></a>';
	elseif ($field->label == "Title"):
		print '<a href="' . $path .  '">' . $field->content . '</a>';
	elseif ($field->label == "Body"):

		// Functionality to present maximum 500 characters in the Body
		if (strlen($field->content) <= 500):
			print $field->content;
		else:
			print substr($field->content, 0, 500) . ' ... ';
		endif;
	else:
		print $field->content;
	endif;
	
endforeach;

// Closes the Column Div
print '</div>';

 // If the final row has being printed, it closes the Row and Container Divs
if ($current_row == $total_rows):
	 print '</div></div>';
endif;

?>
