<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the	 field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */

 if (isset($_SESSION['kma']['slideImageCounter']) == FALSE):
	drupal_session_start();
	$_SESSION['kma']['slideImageCounter'] = 0;
	$_SESSION['kma']['isFirstImage'] = TRUE;
endif;


// Gets the current row of the view
$counter = $_SESSION['kma']['slideImageCounter'] + 1;

$isContentSlideImage = false;
$isFirstImage = $_SESSION['kma']['isFirstImage'];

$showSlogan = drupal_is_front_page();

foreach ($fields as $id => $field):

	// Gets the Section where the Image should be printed
	if ($field->label == "Site Section"):
		$slideSection = str_replace("&#039;", "'", removeHTMLTags($field->content, 1));
		$isContentSlideImage = isCurrentSiteInSection($slideSection, drupal_is_front_page(), menu_get_active_title());
	endif;


	if ($isContentSlideImage):

		$_SESSION['kma']['slideImageCounter'] = $counter;

		// Gets the Image URL and Title
		if ($field->label == "Image"):
			$imageLink = file_create_url($row->field_field_image[0]['rendered']['#item']['uri']);
		elseif ($field->label == "Slogan" && $showSlogan):
			$imageText = $field->content;
		endif;

		// Prints only the first image
		if ($isFirstImage):

			if ($field->label == "Image"):

				// Prints the Slide Image Section
				print '<section id="';

				print 'slideimage" class="jumbotron parallax row" style="background-image: url(';

				print $imageLink;
				print ');">';
				print '<input type="hidden" id="slideImageCurrentValue" value="1">';

				$_SESSION['kma']['isFirstImage'] = FALSE;

			elseif ($field->label == "Slogan" && $showSlogan):
				// Prints the title of the Image
				print $imageText;
			endif;
		endif;

		if ($field->label == "Image"):

			if ($counter == 2):
				// Prints the Previous and Next buttons
				print '<div class="carousel-nav"><div class="slideShowButtonsTable" >';
					print '<span>';
						print '<img id="slideImagePreviousButton" src="' . drupal_get_path('theme', 'kma') . '/assets/img/previous_arrow.png"';
						print ' class="slideShowButton"';
						print ' onclick="changePreviousSlideImage();" style="visibility:hidden;">';
					print '</span>';
					print '<span>';
						print '<img id="slideImageNextButton" src="' . drupal_get_path('theme', 'kma') . '/assets/img/next_arrow.png"';
						print ' class="slideShowButton"';
						print ' onclick="changeNextSlideImage();">';
					print '</span>';
				print '</div></div>';
			endif;

			// If it is not the first image, then saves the info in hidden controls
			$inputID = 'slideImageURL' . strval($counter);

			print '<input type="hidden" id="' . $inputID . '" value="' . $imageLink . '">';
		elseif ($field->label == "Slogan" & $showSlogan):
			$imageText = removeHTMLTags($imageText, 2);
			$inputID = 'slideImageText' . strval($counter);
			print '<input type="hidden" id="' . $inputID . '" value="' . $imageText . '">';
		endif;
	endif;
endforeach;
print "</section>";

?>
