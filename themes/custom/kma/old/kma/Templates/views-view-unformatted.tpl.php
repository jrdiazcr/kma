<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 *
 * This template file was created to remove unwanted <Divs> between the Rows in the Practice Areas View 
 * 
 * views-view-unformatted--practice-areas.tpl.php
 *  */
 

?>
<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif;

foreach ($rows as $id => $row):
    print $row; 
endforeach; 

if (isset($_SESSION['kma']['slideImageCounter'])):
	$_SESSION['kma']['slideImageCounter'] = 0;
	$_SESSION['kma']['isFirstImage'] = TRUE;
endif;

?>
