
var timeSeconds = 5;
var timer = setInterval("changeCurrentSlideImage()", timeSeconds * 1000);

//google.maps.event.addDomListener(window, 'load', initialize);


function initialize()
{	
	// Load contact us maps
	//loadContactUsMap();
}


function changeCurrentSlideImage()
{
	if (document.getElementById('slideimage') != null)
	{
		currentValue = parseInt(document.getElementById('slideImageCurrentValue').value) + 1;
		changeSlideImage(currentValue);
	}
}

function changePreviousSlideImage()
{
	nextValue = parseInt(document.getElementById('slideImageCurrentValue').value) - 1;
	changeSlideImage(nextValue);
	clearInterval(timer);
	timer = setInterval("changeCurrentSlideImage()", timeSeconds * 1000);
}

function changeNextSlideImage()
{
	nextValue = parseInt(document.getElementById('slideImageCurrentValue').value) + 1;
	changeSlideImage(nextValue);
	clearInterval(timer);
	timer = setInterval("changeCurrentSlideImage()", timeSeconds * 1000);
}

function changeSlideImage(slideImageNumber)
{
	slideImageURL = "slideImageURL" + slideImageNumber.toString();
	slideImageText = "slideImageText" + slideImageNumber.toString();
	slideImageElement = document.getElementById('slideimage');

	if (document.getElementById(slideImageURL) == null)
	{
		slideImageNumber = 1;
		slideImageURL = "slideImageURL1";
		slideImageText = "slideImageText1";
	}

	document.getElementById('slideImageCurrentValue').value = slideImageNumber.toString();
	document.getElementById('slideimage').style.backgroundImage = "url('" + document.getElementById(slideImageURL).value + "')";

	for (i = 0; i < slideImageElement.childNodes.length; i++)
	{
		if (slideImageElement.childNodes[i].nodeName=="H1")
		{
			slideImageElement.childNodes[i].innerHTML = document.getElementById(slideImageText).value;		
		}
	}

	if (slideImageNumber == 1)
	{
		document.getElementById('slideImagePreviousButton').style.visibility = 'hidden';
	}
	else
	{
		document.getElementById('slideImagePreviousButton').style.visibility = 'visible';
	}

	nextSlideImageNumber = slideImageNumber + 1;
	nextSlideImageURL = "slideImageURL" + nextSlideImageNumber.toString();

	if (document.getElementById(nextSlideImageURL) == null)
	{
		document.getElementById('slideImageNextButton').style.visibility = 'hidden';
	}
	else
	{
		document.getElementById('slideImageNextButton').style.visibility = 'visible';
	}

}


// Load the maps for the contact us section
/*
function loadContactUsMap(){
	var mapCanvas1 = document.getElementById('kam_address_map1');

	if (mapCanvas1 != null)
	{
		var mapCanvas2 = document.getElementById('kam_address_map2');
		var mapCanvas3 = document.getElementById('kam_address_map3');

		var mapLatlng1 = new google.maps.LatLng(37.798099, -122.3989431);
		var mapLatlng2 = new google.maps.LatLng(34.0501822, -118.2539201);
		var mapLatlng3 = new google.maps.LatLng(32.7205531, -117.1679138);

		var mapOptions1 =
		{
			center: mapLatlng1,
			zoom: 17,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		}

		var mapOptions2 =
		{
			center: mapLatlng2,
			zoom: 17,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		}

		var mapOptions3 =
		{
			center: mapLatlng3,
			zoom: 17,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		}

		var map1 = new google.maps.Map(mapCanvas1, mapOptions1);
		var map2 = new google.maps.Map(mapCanvas2, mapOptions2);
		var map3 = new google.maps.Map(mapCanvas3, mapOptions3);
		
		
		var marker1 = new google.maps.Marker({
		      position: mapLatlng1,
		      map: map1,
		      title: 'KMA Location'
		  });

		var marker2 = new google.maps.Marker({
		      position: mapLatlng2,
		      map: map2,
		      title: 'KMA Location'
		  });

		var marker3 = new google.maps.Marker({
		      position: mapLatlng3,
		      map: map3,
		      title: 'KMA Location'
		  });		
	}	
	
}

*/