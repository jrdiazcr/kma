<?php

function kma_preprocess_image(&$variables) {
  $attributes = &$variables['attributes'];

  // Adds the class img-responsive
  foreach (array('class') as $key) {
    unset($attributes[$key]);
    unset($variables[$key]);
	$variables['attributes']['class'][] = 'img-responsive'; 
  }
  
  // Removes Width and Height from the images
  foreach (array('width', 'height') as $key) {
    unset($attributes[$key]);
    unset($variables[$key]);
  }
}

  
function kma_preprocess_html($vars) {
	drupal_add_js('https://maps.googleapis.com/maps/api/js', 'external');	
			
	drupal_add_css(drupal_get_path('theme', 'kma') ."/assets/css/fontello.css");
	drupal_add_css(drupal_get_path('theme', 'kma') ."/assets/css/bootstrap.min.css");
	
	if(drupal_is_front_page()) {	  	
		drupal_add_css(drupal_get_path('theme', 'kma') ."/assets/css/master.css");    
	} else{
		drupal_add_css(drupal_get_path('theme', 'kma') ."/assets/css/master_secondary.css");
	}	
	
	if (isset($_SESSION['kma']['slideImageCounter']) == FALSE):
		drupal_session_start();
		$_SESSION['kma']['slideImageCounter'] = 0;
		$_SESSION['kma']['isFirstImage'] = TRUE;
	endif;
	
	$_SESSION['kma']['bio_office'] = '';
	$_SESSION['kma']['bio_office_qty'] = '1';
	$_SESSION['kma']['client_type'] = '';
	$_SESSION['kma']['client_type_qty'] = '1';
}


function kma_preprocess_page(&$vars) {
	 if (isset($vars['node']->type)) {
	 	 $vars['theme_hook_suggestions'][] = 'page__' . $vars['node']->type; 
	 } 
	 
  $variables['custom_menu'] = menu_navigation_links('menu-custom-menu');
}

function kma_menu_tree(array $variables) {
		
	$pos1 = stripos($variables['tree'], '<ul ');
	
	$classes = '';
	
	if ($pos1 == FALSE):
		$classes = 'dropdown-menu';
	else:
		$classes = 'nav navbar-nav pull-right my-nav';
	endif;
	
  return '<ul class="' . $classes . '">' . $variables['tree'] . '</ul>';
}

function kma_menu_link(array $variables) {
  $element = $variables['element'];
  $sub_menu = '';  
  
  // Unset all the classes
  unset($element['#attributes']['class']);

  if ($element['#below']) {
  	$element['#attributes']['class'][] = 'dropdown';
   
    $sub_menu = drupal_render($element['#below']);
  }
  else{
  	$element['#attributes']['class'][] = '';
  }

  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}
